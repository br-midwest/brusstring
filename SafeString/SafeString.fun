(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: SafeString
 * File: SafeString.fun
 * Author: Matt Adams, B&R USA
 * Created: May 08, 2013
 ********************************************************************
 * Functions and function blocks of library SafeString
 ********************************************************************)

FUNCTION strcatsafe : BOOL (*Safe String Concat, with check for length overruns.*) (*$GROUP=User*)
	VAR_INPUT
		pDest : UDINT; (*Address of Destination String*)
		pSrc : UDINT; (*Address of Source String*)
		DestSize : UDINT; (*SIZEOF Destination*)
		LogLevel : LogLevel_typ; (*Logging Required on Overrun (See Enumeration for Types)*)
	END_VAR
	VAR
		DestLen : UINT;
		SrcLen : UINT;
		LogEntry : STRING[255];
	END_VAR
END_FUNCTION
