(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: SafeString
 * File: SafeString.typ
 * Author: Matt Adams, B&R USA
 * Created: May 08, 2013
 ********************************************************************
 * Data types of library SafeString
 ********************************************************************)

TYPE
	LogLevel_typ : 
		(
		LogLevelNone, (*No logging in case of overrun*)
		LogLevelWarning, (*A warning is logged in system logger in case of overrun*)
		LogLevelFatal (*A fatal error is logged in system logger and system reboots into service mode*)
		);
END_TYPE
