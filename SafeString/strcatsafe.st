(********************************************************************
 * Library: SafeString
 * File: strcatsafe.st
 * Author: Matt Adams, B&R USA
 * Created: May 08, 2013
 ********************************************************************
 * Implementation of library SafeString
 ********************************************************************) 

(* Safe String Concatenate, with check for length overruns. *)
(* This function is passed the destination and source addresses, as well as the overall size of the destination, assumed to be a string. The returned SIZEOF value in IEC
   includes the extra byte for the null terminator. Any copies here will ensure a copy does not exceed the declared size. e.g. a STRING[15] will not have more than 15 characters. *)

FUNCTION strcatsafe
	
	// Get the size of the destination and source strings.
	DestLen := strlen(pDest);
	SrcLen 	:= strlen(pSrc);
	
	IF DestSize-1 > DestLen THEN // Room is left over to concatenate to at least part of a the string 	
		IF DestSize > DestLen + SrcLen THEN // No problems, do a normal strcat
			strcat(pDest,pSrc);
			strcatsafe := TRUE;
		ELSIF DestSize < DestLen + SrcLen  THEN	// Not enough room to copy all of the source string, do a partial copy				
			IF LogLevel <> LogLevelNone THEN // Log as a warning or fatal.
				memset(ADR(LogEntry),0,SIZEOF(LogEntry));
				strcpy(ADR(LogEntry),ADR('strcatsafe string overrun: '));
				memcpy(ADR(LogEntry)+strlen(ADR(LogEntry)),pDest, MIN(DestLen,SIZEOF(LogEntry)-strlen(ADR(LogEntry))));
				memcpy(ADR(LogEntry)+strlen(ADR(LogEntry)),ADR(' can not fit '), MIN(13,SIZEOF(LogEntry)-strlen(ADR(LogEntry))));
				memcpy(ADR(LogEntry)+strlen(ADR(LogEntry)),pSrc, MIN(SrcLen,SIZEOF(LogEntry)-strlen(ADR(LogEntry))));	
				IF LogLevel = LogLevelWarning THEN
					ERRxwarning(55023,(DestSize-1)-DestLen,ADR(LogEntry));
				ELSIF LogLevel = LogLevelFatal THEN
					ERRxfatal(55023,(DestSize-1)-DestLen,ADR(LogEntry));
				END_IF		
			END_IF			
			memcpy(pDest+DestLen,pSrc,(DestLen+SrcLen)-(DestSize-1));
			strcatsafe := FALSE;
		END_IF
	ELSE // The string is already full up to the max length
		IF LogLevel <> LogLevelNone THEN
			memset(ADR(LogEntry),0,SIZEOF(LogEntry));
			strcpy(ADR(LogEntry),ADR('strcatsafe string full: '));
			memcpy(ADR(LogEntry)+strlen(ADR(LogEntry)),pDest, MIN(DestLen,SIZEOF(LogEntry)-strlen(ADR(LogEntry))));
			memcpy(ADR(LogEntry)+strlen(ADR(LogEntry)),ADR(' can not add '), MIN(13,SIZEOF(LogEntry)-strlen(ADR(LogEntry))));
			memcpy(ADR(LogEntry)+strlen(ADR(LogEntry)),pSrc, MIN(DestLen,SIZEOF(LogEntry)-strlen(ADR(LogEntry))));		
			IF LogLevel = LogLevelWarning THEN
				ERRxwarning(55024,(DestSize-1)-DestLen,ADR(LogEntry));
			ELSIF LogLevel = LogLevelFatal THEN
				ERRxfatal(55024,(DestSize-1)-DestLen,ADR(LogEntry));
			END_IF
		END_IF
		strcatsafe := FALSE;
	END_IF
	
END_FUNCTION