v1.01.0 - added strARL library to do printf type functions.  Finds keyword, pulls value of variable out of array and replaces keyword with var value. Tim Kulchawick 

v1.00.0 - stristr find string in string function. Matt Adams 