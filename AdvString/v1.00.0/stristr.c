/********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: AdvString
 * File: stristr.c
 * Author: adamsm
 * Created: July 29, 2013
 ********************************************************************
 * Implementation of library AdvString
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "AdvString.h"
	#include "string.h"

#ifdef __cplusplus
	};
#endif

/* Case Insensitive String Match */
void stristr(struct stristr* inst)
{
	strcpy(&inst->HaystackArray,inst->pHaystack);
	strcpy(&inst->NeedleArray,inst->pNeedle);
	for (inst->i=0; inst->i<sizeof(inst->NeedleArray); inst->i++) 
	{
		if (inst->NeedleArray[inst->i] == 0)
		{
			break;	
		}
		
		if (inst->NeedleArray[inst->i] >= 65 && inst->NeedleArray[inst->i] <= 90)
		{
			inst->NeedleArray[inst->i] = inst->NeedleArray[inst->i] + 32;
		}
	}
	
	for (inst->i=0; inst->i<sizeof(inst->HaystackArray); inst->i++) 
	{
		if (inst->HaystackArray[inst->i] == 0)
		{
			break;	
		}
			
		if (inst->HaystackArray[inst->i] >= 65 && inst->HaystackArray[inst->i] <= 90)
		{
			inst->HaystackArray[inst->i] = inst->HaystackArray[inst->i] + 32;
		}
		
	}
	
	inst->result = strstr(&inst->HaystackArray,&inst->NeedleArray);
	
	if (inst->result >0)
	{
		inst->Match = 1;	
	}
	else
	{
		inst->Match = 0;
	}
	
}
