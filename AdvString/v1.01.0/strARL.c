
#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
#include "AdvString.h"
#ifdef __cplusplus
	};
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

/* Define some macros */
#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif
#ifndef ARRAY_ENTRIES
#define ARRAY_ENTRIES(array) (sizeof(array)/sizeof(array[0]))
#endif

/* Define boolean states */
#ifndef TRUE
#define TRUE  1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/* Define default error constants (if not defined before) */
#ifndef ERR_OK
#define ERR_OK                0
#endif
#ifndef ERR_FUB_ENABLE_FALSE
#define ERR_FUB_ENABLE_FALSE  65534
#endif
#ifndef ERR_FUB_BUSY
#define ERR_FUB_BUSY          65535
#endif

/* ID base value */
#define BRSE_ARL_ID_BASE 0x53450000

/* Define error constants */
#define BRSE_ARL_ID_ALREADY_INITIALIZED	58200
#define BRSE_ARL_ID_NOT_VALID 			58201
#define BRSE_ARL_BUFFER_FULL  			58202
#define BRSE_ARL_OUT_OF_MEMORY          58203


plcbit strARL(unsigned char* dst, unsigned long dst_size, unsigned char* src, struct Args_typ* pArgs)
{
    
    /* Temporary array for string concatenations */
    char temp_string[16];

    /* Size of a string that is concatenated to the destination */
    int concat_string_size;
	
    /* Array index counters */
    int bool_counter   = 0;		
    int int_counter    = 0;
    int real_counter   = 0;
    int string_counter = 0;

    /* Run through the source message and copy it to the destination until src is empty or destination is full */
    while ((*src != '\0') && (dst_size > 0))
    {
        /* Special character found and MessageData given */
        if ((pArgs != NULL) && (*src == '%'))
        {
            /* Temporary 'close' the destination string (to allow strcat...) */
            *dst = '\0';
		
            /* Consume the char and investigate the command */
            switch (*(++src))
            {
                /* Insert bool */
                case 'b':
                    /* Only take a value if an array entry exists */
                    if (bool_counter < ARRAY_ENTRIES(pArgs->b))
                    {
                        if (pArgs->b[bool_counter])
                        {
                            strncat(dst, "TRUE", dst_size);
                            concat_string_size = MIN(dst_size, 4);
                        }
                        else
                        {
                            strncat(dst, "FALSE", dst_size);
                            concat_string_size = MIN(dst_size, 5);
                        }
                        bool_counter++;
                        dst += concat_string_size;
                        dst_size -= concat_string_size;
                    }
                    src++;
                    break;
					
                /* Insert integer */
                case 'i':
                    /* Only take a value if an array entry exists */
                    if (int_counter < ARRAY_ENTRIES(pArgs->i))
                    {
                        concat_string_size = brsitoa(pArgs->i[int_counter], (UDINT)temp_string);
                        concat_string_size = MIN(dst_size, concat_string_size);
                        strncat(dst, temp_string, dst_size);
                        int_counter++;
                        dst += concat_string_size;
                        dst_size -= concat_string_size;
                    }
                    src++;
                    break;
					
                /* Insert real */
                case 'r':
                    /* Only take a value if an array entry exists */
                    if (real_counter < ARRAY_ENTRIES(pArgs->r))
                    {
                      
                        concat_string_size = brsftoa(pArgs->r[real_counter], (UDINT)temp_string);
                    
                        //                        strcpy(&temp_string,"test");
                        //                        concat_string_size = 4;
                    
                        concat_string_size = MIN(dst_size, concat_string_size);
                        strncat(dst, temp_string, dst_size);
                        real_counter++;
                        dst += concat_string_size;
                        dst_size -= concat_string_size;
                    }
                    src++;
                    break;
					
                /* Insert string */
                case 's':
                    /* Only take a value if an array entry exists */
                    if (string_counter < ARRAY_ENTRIES(pArgs->s))
                    {
                        if (pArgs->s[string_counter]) 
                        {
                            strncat(dst, (char*)pArgs->s[string_counter], dst_size);
                            concat_string_size = MIN(dst_size, strlen((char*)pArgs->s[string_counter]));
                            dst += concat_string_size;
                            dst_size -= concat_string_size;
                        }
                        string_counter++;
                    }
                    src++;
                    break;
					
                /* Insert percent */
                case '%':
                    *dst++ = '%';
                    src++;
                    dst_size--;
                    break;
					
                /* ELSE */
                default:
                    /* Ignore the character */
                    src++;
                    break;
            }
        }
        else
        {
            *dst++ = *src++;
            dst_size--;
        }
    }
	
    /* Add the end of string */
    *dst = '\0';
    
}
