(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: AdvString
 * File: AdvString.fun
 * Author: adamsm
 * Created: July 29, 2013
 ********************************************************************
 * Functions and function blocks of library AdvString
 ********************************************************************)

FUNCTION_BLOCK stristr (*Case Insensitive String Match*) (*$GROUP=User*)
	VAR_INPUT
		pHaystack : UDINT;
		pNeedle : UDINT;
	END_VAR
	VAR_OUTPUT
		Match : BOOL;
	END_VAR
	VAR
		HaystackArray : ARRAY[0..1023] OF USINT;
		NeedleArray : ARRAY[0..1023] OF USINT;
		i : UINT;
		result : UDINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION strARL : BOOL (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		dst : REFERENCE TO USINT;
		dst_size : UDINT;
		src : REFERENCE TO USINT;
		pArgs : Args_typ;
	END_VAR
END_FUNCTION
