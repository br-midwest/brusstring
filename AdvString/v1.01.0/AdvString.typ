(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: AdvString
 * File: AdvString.typ
 * Author: adamsm
 * Created: July 29, 2013
 ********************************************************************
 * Data types of library AdvString
 ********************************************************************)

TYPE
	Args_typ : 	STRUCT 
		r : ARRAY[0..4]OF REAL;
		s : ARRAY[0..4]OF UDINT;
		b : ARRAY[0..4]OF BOOL;
		i : ARRAY[0..4]OF DINT;
	END_STRUCT;
END_TYPE
