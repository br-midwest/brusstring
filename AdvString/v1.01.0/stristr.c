/********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: AdvString
 * File: stristr.c
 * Author: adamsm
 * Created: July 29, 2013
 ********************************************************************
 * Implementation of library AdvString
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "AdvString.h"
	#include "string.h"

#ifdef __cplusplus
	};
#endif

/* Case Insensitive String Match */
void stristr(struct stristr* inst)
{
	strcpy(&inst->HaystackArray,inst->pHaystack);  //strcpy the haystack (already address) into address of haystack array
	strcpy(&inst->NeedleArray,inst->pNeedle);  //strcpy Needle (already address) into address of Needle Array
	for (inst->i=0; inst->i<sizeof(inst->NeedleArray); inst->i++)  //i=0, i < sizeof NeedleArray, i++
	{
		if (inst->NeedleArray[inst->i] == 0)  //if null character
		{
			break;	
		}
		
		if (inst->NeedleArray[inst->i] >= 65 && inst->NeedleArray[inst->i] <= 90) //if uppercase
		{
			inst->NeedleArray[inst->i] = inst->NeedleArray[inst->i] + 32;	//shift to make all char lowercase
		}
	}
	
	for (inst->i=0; inst->i<sizeof(inst->HaystackArray); inst->i++) 
	{
		if (inst->HaystackArray[inst->i] == 0)  //if null character
		{
			break;	
		}
			
		if (inst->HaystackArray[inst->i] >= 65 && inst->HaystackArray[inst->i] <= 90)  //if uppercase
		{
			inst->HaystackArray[inst->i] = inst->HaystackArray[inst->i] + 32; //shift by 32 to make all letters lowercase
		}
		
	}
	
	inst->result = strstr(&inst->HaystackArray,&inst->NeedleArray);  //result is a pointer to the first occurance of needle in the haystack.  Standard C string library function
	
	if (inst->result >0)
	{
		inst->Match = 1;	
	}
	else
	{
		inst->Match = 0;
	}
	
}