
FUNCTION memcmp_s : INT
	VAR_INPUT
		pMem1 : UDINT;
		Mem1Max : UDINT;
		pMem2 : UDINT;
		length : UDINT;
		pRes : UDINT;
	END_VAR
END_FUNCTION

FUNCTION memcpy_s : INT
	VAR_INPUT
		pDest : UDINT;
		DestMax : UDINT;
		pSrc : UDINT;
		length : UDINT;
	END_VAR
END_FUNCTION

FUNCTION memmove_s : INT
	VAR_INPUT
		pDest : UDINT;
		DestMax : UDINT;
		pSrc : UDINT;
		length : UDINT;
	END_VAR
END_FUNCTION

FUNCTION memset_s : INT
	VAR_INPUT
		pDest : UDINT;
		value : USINT;
		length : UDINT;
	END_VAR
END_FUNCTION

FUNCTION strcat_s : INT
	VAR_INPUT
		pDest : UDINT;
		DestMax : UDINT;
		pSrc : UDINT;
	END_VAR
END_FUNCTION

FUNCTION strcpy_s : INT
	VAR_INPUT
		pDest : UDINT;
		DestMax : UDINT;
		pSrc : UDINT;
	END_VAR
END_FUNCTION

FUNCTION strcmp_s : INT
	VAR_INPUT
		pDest : UDINT;
		DestMax : UDINT;
		pSrc : UDINT;
		pRes : UDINT;
	END_VAR
END_FUNCTION

FUNCTION strnlen_s : UDINT
	VAR_INPUT
		pDest : UDINT;
		DestMax : UDINT;
	END_VAR
END_FUNCTION
