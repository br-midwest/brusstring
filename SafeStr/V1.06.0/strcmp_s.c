/*------------------------------------------------------------------
 * strcmp_s.c -- string compare
 *
 * November 2008, Bo Berry
 *
 * Copyright (c) 2008-2011 by Cisco Systems, Inc
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *------------------------------------------------------------------
 */
/*******************************************************
* COPYRIGHT: B&R Industrial Automation
********************************************************
* Author:	Alex Pauls, Matt Adams
* Created:	April 8, 2020 
********************************************************
[program information]:
V1.0 - Safer replacement for strcmp that avoids 
		vulnerabilities (e.g. buffer overflows, string 
		format attacks, conversion overflows/underflows,
		etc.).
V1.2 - MJA: Included error handling with unified safe_str_constraint which causes
	   service mode in case of memory violations.
********************************************************/
#include "safeclib_private.h"
#include "SafeStr.h"

/**
 * NAME
 *    strcmp_s
 *
 * Synpsos
 *    #include "safe_str_lib.h"
 *    errno_t
 *    strcmp_s(const char *dest, rsize_t dmax,
 *             const char *src, int *indicator)
 *
 * DESCRIPTION
 *    Compares string src to string dest.
 *
 * EXTENSION TO
 *    ISO/IEC JTC1 SC22 WG14 N1172, Programming languages, environments
 *    and system software interfaces, Extensions to the C Library,
 *    Part I: Bounds-checking interfaces
 *
 * INPUT PARAMETERS
 *    pDest       pointer to string to compare against
 *
 *    DestMax     restricted maximum length of string dest
 *
 *    pSrc        pointer to the string to be compared to dest
 *
 *    pRes  	  pointer to result indicator, greater than,
 *               equal to or less than 0, if the string pointed
 *               to by dest is greater than, equal to or less
 *               than the string pointed to by src respectively.
 *
 * OUTPUT PARAMETERS
 *    indicator  updated result indicator
 *
 * RUNTIME CONSTRAINTS
 *    Neither dest nor src shall be a null pointer.
 *    indicator shall not be a null pointer.
 *    dmax shall not be 0
 *    dmax shall not be greater than RSIZE_MAX_STR
 *
 * RETURN VALUE
 *    indicator, when the return code is OK
 *        >0   dest greater than src
 *         0   strings the same
 *        <0   dest less than src
 *
 *    EOK
 *    ESNULLP     pointer was null
 *    ESZEROL     length was zero
 *    ESLEMAX     length exceeded max
 *
 * ALSO SEE
 *    strcasecmp_s()
 *
 */
INT strcmp_s (UDINT pDest, UDINT DestMax, UDINT pSrc, UDINT pRes)
{
	uint8_t *dp;
	const uint8_t  *sp;
	uint16_t *rp;

	dp = (uint8_t *) pDest;
	sp = (uint8_t *) pSrc;
	rp = (uint16_t *) pRes;
	
    if (rp == NULL) {
        return SAFESTR_NULL_POINTER;
    }
    *rp = 0;

    if (dp == NULL) {
        return SAFESTR_NULL_POINTER;
    }

    if (sp == NULL) {
        return SAFESTR_NULL_POINTER;
    }

    if (DestMax == 0) {
        return SAFESTR_ZERO_LEN;
    }

    if (DestMax > RSIZE_MAX_STR) {
        return SAFESTR_LEN_EXCEEDS_MAX;
    }

    while (*dp && *sp && DestMax) {

        if (*dp != *sp) {
            break;
        }

        dp++;
        sp++;
        DestMax--;
    }

    *rp = *dp - *sp;
    return SAFESTR_OK;
}
EXPORT_SYMBOL(strcmp_s)
