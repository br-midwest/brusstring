/*------------------------------------------------------------------
 * memcpy_s
 *
 * October 2008, Bo Berry
 *
 * Copyright (c) 2008-2011 Cisco Systems
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *------------------------------------------------------------------
 */

/*******************************************************
* COPYRIGHT: B&R Industrial Automation
********************************************************
* Author:	Alex Pauls, Matt Adams
* Created:	April 8, 2020 
********************************************************
[program information]:
V1.0 - Safer replacement for memcmp that avoids 
		vulnerabilities (e.g. buffer overflows, string 
		format attacks, conversion overflows/underflows,
		etc.).
V1.2 - MJA: Included error handling with unified safe_str_constraint which causes
	   service mode in case of memory violations.
********************************************************/
#include <bur/plctypes.h>
#include "safeclib_private.h"
#include "SafeStr.h"
#include "safe_str_constraint.h"



/**
 * NAME
 *    memcmp_s
 *
 * SYNOPSIS
 *    #include "safe_mem_lib.h"
 *    errno_t
 *    memcmp_s(const void *pMem1, rsize_t Mem1Max,
 *             const void *pMem2,  rsize_t Mem2Max, int *pRes)
 *
 * DESCRIPTION
 *    Compares memory until they differ, and their difference is
 *    returned in pRes.  If the block of memory is the same, pRes=0.
 *
 * EXTENSION TO
 *    ISO/IEC JTC1 SC22 WG14 N1172, Programming languages, environments
 *    and system software interfaces, Extensions to the C Library,
 *    Part I: Bounds-checking interfaces
 *
 * INPUT PARAMETERS
 *    pMem1      pointer to memory to compare against
 *
 *    Mem1Max      maximum length of pMem1, in bytess
 *
 *    pMem2       pointer to the source memory to compare with pMem1
 *
 *    length      length of the source memory block
 *
 *    pRes     pointer to the pRes which is an integer greater
 *              than, equal to or less than zero according to
 *              whether the object pointed to by pMem1 is
 *              greater than, equal to or less than the object
 *              pointed to by pMem2.
 *
 *  OUTPUT PARAMETERS
 *    none
 *
 * RUNTIME CONSTRAINTS
 *    Neither pMem1 nor pMem2 shall be a null pointer.
 *    Neither Mem1Max nor Mem2Max shall be zero.
 *    Mem1Max shall not be greater than RSIZE_MAX_MEM.
 *    Mem2Max shall not be greater than Mem1Max.
 *
 * RETURN VALUE
 *    EOK        successful operation
 *    ESNULLP    NULL pointer
 *    ESZEROL    zero length
 *    ESLEMAX    length exceeds max limit
 *
 * ALSO SEE
 *    memcmp16_s(), memcmp32_s()
 *
 */
INT memcmp_s(UDINT pMem1, UDINT Mem1Max, UDINT pMem2,  UDINT Mem2Max, UDINT pRes)
{
	const uint8_t *dp;
	const uint8_t *sp;
	uint8_t *rp;

	dp = (uint8_t *)pMem1;
	sp = (uint8_t *)pMem2;
	rp = (uint8_t *)pRes;

    /*
     * must be able to return the pRes
     */
	if (rp == NULL) {
		return SAFESTR_NULL_POINTER;
	}
	*rp = -1;  /* default pRes */

	if (dp == NULL) {
		return SAFESTR_NULL_POINTER;
	}

	if (sp == NULL) {
		return SAFESTR_NULL_POINTER;
	}

	if (Mem1Max == 0) {
		return SAFESTR_ZERO_LEN;
	}

	if (Mem1Max > RSIZE_MAX_MEM) {
		return SAFESTR_LEN_EXCEEDS_MAX;
	}

	if (Mem2Max == 0) {
		return SAFESTR_ZERO_LEN;
	}

	if (Mem2Max > Mem1Max) {
		return (SAFESTR_LEN_EXCEEDS_MAX);
	}

    /*
     * no need to compare the same memory
     */
	if (dp == sp) {
		*rp = 0;
		return SAFESTR_OK;
	}

    /*
     * now compare sp to dp
     */
	*rp = 0;
	while (Mem1Max > 0 && Mem2Max > 0) {
		if (*dp != *sp) {
			/***  *pRes = *dp - *sp; ***/
			*rp = *dp < *sp ? -1 : 1;
			break;
		}

		Mem1Max--;
		Mem2Max--;

		dp++;
		sp++;
	}

	return SAFESTR_OK;
}
EXPORT_SYMBOL(memcmp_s)

