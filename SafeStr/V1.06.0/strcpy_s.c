/*------------------------------------------------------------------
 * strcpy_s.c
 *
 * October 2008, Bo Berry
 *
 * Copyright (c) 2008-2011 by Cisco Systems, Inc
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *------------------------------------------------------------------
 */
/*******************************************************
* COPYRIGHT: B&R Industrial Automation
********************************************************
* Author:	Alex Pauls, Matt Adams
* Created:	April 8, 2020 
********************************************************
[program information]:
V1.0 - Safer replacement for strcpy that avoids 
		vulnerabilities (e.g. buffer overflows, string 
		format attacks, conversion overflows/underflows,
		etc.).
V1.1 - subtracts 1 from DestMax in order to allow SIZEOF() to be 
       used on strings for simple implementation.
V1.2 - MJA: Included error handling with unified safe_str_constraint which causes
	   service mode in case of memory violations. Returns DestMax to typical size
********************************************************/
#include "safeclib_private.h"
#include "SafeStr.h"
#include "safe_str_constraint.h"

/**
 * NAME
 *    strcpy_s
 *
 * SYNOPSIS
 *    #include "safe_str_lib.h"
 *    errno_t
 *    strcpy_s(char *pDest, rsize_t DestMax, const char *pSrc)
 *
 * DESCRIPTION
 *    The strcpy_s function copies the string pointed to by pSrc
 *    (including the terminating null character) into the array
 *    pointed to by pDest. All elements following the terminating
 *    null character (if any) written by strcpy_s in the array
 *    of DestMax characters pointed to by pDest are nulled when
 *    strcpy_s returns.
 *
 * SPECIFIED IN
 *    ISO/IEC TR 24731, Programming languages, environments
 *    and system software interfaces, Extensions to the C Library,
 *    Part I: Bounds-checking interfaces
 *
 * INPUT PARAMETERS
 *    pDest      pointer to string that will be replaced by pSrc.
 *
 *    DestMax      restricted maximum length of pDest
 *
 *    pSrc       pointer to the string that will be copied
 *               to pDest
 *
 * OUTPUT PARAMETERS
 *    pDest      updated
 *
 * RUNTIME CONSTRAINTS
 *    Neither pDest nor pSrc shall be a null pointer.
 *    DestMax shall not be greater than RSIZE_MAX_STR.
 *    DestMax shall not equal zero.
 *    DestMax shall be greater than strnlen_s(pSrc, DestMax).
 *    Copying shall not take place between objects that overlap.
 *    If there is a runtime-constraint violation, then if pDest
 *       is not a null pointer and destmax is greater than zero and
 *       not greater than RSIZE_MAX_STR, then strcpy_s nulls pDest.
 *
 * RETURN VALUE
 *    EOK        successful operation, the characters in pSrc were
 *               copied into pDest and the result is null terminated.
 *    ESNULLP    NULL pointer
 *    ESZEROL    zero length
 *    ESLEMAX    length exceeds max limit
 *    ESOVRLP    strings overlap
 *    ESNOSPC    not enough space to copy pSrc
 *
 * ALSO SEE
 *    strcat_s(), strncat_s(), strncpy_s()
 *
 */
INT strcpy_s (UDINT pDest, UDINT DestMax, UDINT pSrc)
{
	rsize_t orig_dmax;
	uint8_t *dp;
	const uint8_t  *sp;
    unsigned char *orig_dest;
    const unsigned char *overlap_bumper;

	dp = (uint8_t *)pDest;
	sp = (uint8_t *)pSrc;
	
    if (dp == NULL) {
       invoke_safe_str_constraint_handler("strcpy_s: pDest is null", NULL, SAFESTR_NULL_POINTER);
        return SAFESTR_NULL_POINTER;
    }

    if (DestMax == 0) {
        invoke_safe_str_constraint_handler("strcpy_s: DestMax is 0", NULL, SAFESTR_ZERO_LEN);
        return SAFESTR_ZERO_LEN;
    }

    if (DestMax > RSIZE_MAX_STR) {
		invoke_safe_str_constraint_handler("strcpy_s: DestMax exceeds max", NULL, SAFESTR_LEN_EXCEEDS_MAX);
        return SAFESTR_LEN_EXCEEDS_MAX;
    }
		
    if (sp == NULL) {
        *dp = '\0';
        invoke_safe_str_constraint_handler("strcpy_s: pSrc is null", NULL, SAFESTR_NULL_POINTER);
        return SAFESTR_NULL_POINTER;
    }

    if (dp == sp) {
        return SAFESTR_OK;
    }

    /* hold base of pDest in case pSrc was not copied */
    orig_dmax = DestMax;
    orig_dest = dp;

    if (dp < sp) {
        overlap_bumper = sp;

        while (DestMax > 0) {
            if (dp == (uint8_t *) overlap_bumper) {
                handle_error((UDINT) orig_dest, orig_dmax, "strcpy_s: overlapping objects", SAFESTR_OVERLAP_UNDEF);
                return SAFESTR_OVERLAP_UNDEF;
            }

            *dp = *sp;
            if (*dp == '\0') {
#ifdef SAFECLIB_STR_NULL_SLACK
                /* null slack to clear any data */
                while (DestMax) { *dp = '\0'; DestMax--; dp++; }
#endif
                return SAFESTR_OK;
            }

            DestMax--;
            dp++;
            sp++;
        }

    } else {
        overlap_bumper = dp;

        while (DestMax > 0) {
            if (sp == (uint8_t *) overlap_bumper) {
                handle_error((UDINT) orig_dest, orig_dmax, "strcpy_s: overlapping objects", SAFESTR_OVERLAP_UNDEF);
                return SAFESTR_OVERLAP_UNDEF;
            }

            *dp = *sp;
            if (*dp == '\0') {
#ifdef SAFECLIB_STR_NULL_SLACK
                /* null slack to clear any data */
                while (DestMax) { *dp = '\0'; DestMax--; dp++; }
#endif
                return SAFESTR_OK;
            }

            DestMax--;
            dp++;
            sp++;
        }
    }

    /*
     * the entire pSrc must have been copied, if not reset pDest to null the string.
     */
	
    handle_error((UDINT)orig_dest, orig_dmax, "strcpy_s: not enough space for pSrc", SAFESTR_NOT_ENOUGH_SPACE);
    return SAFESTR_NOT_ENOUGH_SPACE;
}
EXPORT_SYMBOL(strcpy_s)
