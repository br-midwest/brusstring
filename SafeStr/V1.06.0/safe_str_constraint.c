/*******************************************************
* COPYRIGHT: B&R Industrial Automation
********************************************************
* Author:	Matt Adams, B&R Chicago, USA
* Created:	Nov 11, 2020 
********************************************************
V1.03 - Implemented behavior that shuts down processor with warning messages and pagefault error.
Warnings includes task name and global variable gSafeStrSec into logger
********************************************************/

/*------------------------------------------------------------------
 * safe_str_constraint.h
 *
 * October 2008, Bo Berry
 *
 * Copyright (c) 2008-2011 Cisco Systems
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *------------------------------------------------------------------
 */

#include "safe_str_constraint.h"


// Function used by the libraries to invoke the registered runtime-constraint handler. Always needed.

void invoke_safe_str_constraint_handler(const char *msg, void *ptr, INT error)
{

	char *taskname[20];
	char *group;
	char *logentry[32];
	UDINT sec_pv_adr;
	UDINT data_len;
	DINT SectionData;	
		
	ST_name(0,(char *) &taskname, (unsigned char *)&group);
	
	// gSafeStrSec must be declared global and assigned in order to be used.
	PV_xgetadr((char *)&("gSafeStrSec"), (UDINT *)&sec_pv_adr, (UDINT *)&data_len); 
	if (sec_pv_adr > 0 && data_len > 0) {
		SectionData  = *(DINT *)sec_pv_adr;
		snprintf2((char *)logentry, sizeof(logentry)-1, "Task:%s,Sec:%d", (UDINT)&taskname,SectionData);
	}
	else {
		snprintf2((char *)logentry, sizeof(logentry)-1, "Task: %s", (UDINT)&taskname);
	}
	
	// These can be customized to put additional troubleshooting data
	ERRxwarning(50300, (UDINT) error, (char *)msg);
	ERRxwarning(50301, (UDINT) error, (char *)logentry);
	
	// Causes a page fault on purpose for backtrace data.
	brsmemcpy(1,1,1);
};


// Safe C Lib internal string routine to consolidate error handling
void handle_error(UDINT pDest, rsize_t orig_dmax, char *err_msg, INT err_code)
{
	uint8_t *dp = (uint8_t *) pDest;
	ERRxwarning(50302, orig_dmax, 0);
	*dp = '\0'; // Set destination string to null
	invoke_safe_str_constraint_handler(err_msg,0,err_code);
	return;
}


