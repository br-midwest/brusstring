/*------------------------------------------------------------------
 * memset_s
 *
 * October 2008, Bo Berry
 *
 * Copyright (c) 2008-2011 Cisco Systems
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *------------------------------------------------------------------
 */
/*******************************************************
* COPYRIGHT: B&R Industrial Automation
********************************************************
* Author:	Alex Pauls, Matt Adams
* Created:	April 8, 2020 
********************************************************
[program information]:
V1.0 - Safer replacement for memset that avoids 
		vulnerabilities (e.g. buffer overflows, string 
		format attacks, conversion overflows/underflows,
		etc.).
V1.2 - MJA: Included error handling with unified safe_str_constraint which causes
	   service mode in case of memory violations.
********************************************************/
#include "safeclib_private.h"
#include "mem_primitives_lib.h"
#include "SafeStr.h"
#include "safe_str_constraint.h"

/**
 * NAME
 *    memset_s
 *
 * SYNOPSIS
 *    #include "safe_mem_lib.h"
 *    errno_t
 *    memset_s(void *pDest, rsize_t len, uint8_t value)
 *
 * DESCRIPTION
 *    Sets len bytes starting at pDest to the specified value.
 *
 * SPECIFIED IN
 *    ISO/IEC JTC1 SC22 WG14 N1172, Programming languages, environments
 *    and system software interfaces, Extensions to the C Library,
 *    Part I: Bounds-checking interfaces
 *
 * INPUT PARAMETERS
 *    pDest       pointer to memory that will be set to the value
 *
 *    length        number of bytes to be set
 *
 *    value      byte value
 *
 * OUTPUT PARAMETERS
 *    pDest      is updated
 *
 * RUNTIME CONSTRAINTS
 *    pDest shall not be a null pointer.
 *    length shall not be 0 nor greater than RSIZE_MAX_MEM.
 *    If there is a runtime constraint, the operation is not performed.
 *
 * RETURN VALUE
 *    EOK        successful operation
 *    ESNULLP    NULL pointer
 *    ESZEROL    zero length
 *    ESLEMAX    length exceeds max limit
 *
 * ALSO SEE
 *    memset16_s(), memset32_s()
 *
 */
INT memset_s (UDINT pDest, USINT value, UDINT length)
{
	uint8_t *dp;
	
	dp = (uint8_t *) pDest;
	
    if (dp == NULL) {
        invoke_safe_str_constraint_handler("memset_s: pDest is null", NULL, SAFESTR_NULL_POINTER);
        return SAFESTR_NULL_POINTER;
    }

    if (length == 0) {
        invoke_safe_str_constraint_handler("memset_s: len is 0", NULL, SAFESTR_ZERO_LEN);
        return SAFESTR_ZERO_LEN;
    }

    if (length > RSIZE_MAX_MEM) {
        invoke_safe_str_constraint_handler("memset_s: len exceeds max", NULL, SAFESTR_LEN_EXCEEDS_MAX);
        return (SAFESTR_LEN_EXCEEDS_MAX);
    }

    mem_prim_set((UDINT *) pDest, length, value);

    return SAFESTR_OK;
}
EXPORT_SYMBOL(memset_s)
