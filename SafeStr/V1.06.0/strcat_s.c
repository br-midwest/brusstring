/*------------------------------------------------------------------
 * strcat_s.c
 *
 * October 2008, Bo Berry
 *
 * Copyright (c) 2008-2011 by Cisco Systems, Inc
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *------------------------------------------------------------------

********************************************************
* COPYRIGHT: B&R Industrial Automation
********************************************************
* Author:	Alex Pauls, Matt Adams
* Created:	April 8, 2020 
********************************************************
[program information]:
V1.0 - Safer replacement for strcat that avoids 
		vulnerabilities (e.g. buffer overflows, string 
		format attacks, conversion overflows/underflows,
		etc.).
V1.1 - subtracts 1 from DestMax in order to allow SIZEOF() to be 
       used on strings for simple implementation
********************************************************/
#include "safeclib_private.h"
#include "SafeStr.h"
#include "safe_str_constraint.h"

/**
 * NAME
 *    strcat_s
 *
 * SYNOPSIS
 *    #include "safe_str_lib.h"
 *    errno_t
 *    strcat_s(char *pDest, rsize_t DestMax, const char *pSrc)
 *
 * DESCRIPTION
 *    The strcat_s function appends a copy of the string pointed
 *    to by pSrc (including the terminating null character) to the
 *    end of the string pointed to by pDest. The initial character
 *    from pSrc overwrites the null character at the end ofdest.
 *
 *    All elements following the terminating null character (if
 *    any) written by strcat_s in the array of DestMax characters
 *    pointed to by pDest take unspecified values when strcat_s
 *    returns.
 *
 * SPECIFIED IN
 *    ISO/IEC TR 24731, Programming languages, environments
 *    and system software interfaces, Extensions to the C Library,
 *    Part I: Bounds-checking interfaces
 *
 * INPUT PARAMETERS
 *    pDest      pointer to string that will be extended by pSrc
 *              if DestMax allows. The string is null terminated.
 *              If the resulting concatenated string is less
 *              than DestMax, the remaining slack space is nulled.
 *
 *    DestMax      restricted maximum length of the resulting pDest,
 *              including the null
 *
 *    pSrc       pointer to the string that will be concatenaed
 *              to string pDest
 *
 * OUTPUT PARAMETERS
 *    pDest      is updated
 *
 * RUNTIME CONSTRAINTS
 *    Neither pDest nor pSrc shall be a null pointer
 *    DestMax shall not equal zero
 *    DestMax shall not be greater than RSIZE_MAX_STR
 *    DestMax shall be greater than strnlen_s(pSrc,m).
 *    Copying shall not takeplace between objects that overlap
 *    If there is a runtime-constraint violation, then if pDest is
 *       not a null pointer and DestMax is greater than zero and not
 *       greater than RSIZE_MAX_STR, then strcat_s nulls pDest.
 *
 * RETURN VALUE
 *    EOK        successful operation, all the characters from pSrc
 *                were appended to pDest and the result in pDest is
 *                 null terminated.
 *    ESNULLP    NULL pointer
 *    ESZEROL    zero length
 *    ESLEMAX    length exceeds max
 *    ESUNTERM   pDest not terminated
 *
 * ALSO SEE
 *    strncat_s(), strcpy_s(), strncpy_s()
 *
 */

INT strcat_s (UDINT pDest, UDINT DestMax, UDINT pSrc)
{
	rsize_t orig_dmax;
	uint8_t *dp;
	const uint8_t  *sp;
	const unsigned char *overlap_bumper;
	UDINT orig_dest;
		
	dp = (uint8_t *) pDest;
	sp = (uint8_t *) pSrc;
	
	if (dp == NULL) {
		invoke_safe_str_constraint_handler("strcat_s: pDest = 0", NULL, SAFESTR_NULL_POINTER);
		return SAFESTR_NULL_POINTER;
	}

	if (sp == NULL) {
		invoke_safe_str_constraint_handler("strcat_s: pSrc = 0", NULL, SAFESTR_NULL_POINTER);
		return SAFESTR_NULL_POINTER;
	}

	if (DestMax == 0) {
		invoke_safe_str_constraint_handler("strcat_s: DestMax = 0", NULL, SAFESTR_ZERO_LEN);
		return SAFESTR_ZERO_LEN;
	}

	if (DestMax > RSIZE_MAX_STR) {
		invoke_safe_str_constraint_handler("strcat_s: DestMax > max", NULL, SAFESTR_LEN_EXCEEDS_MAX);
		return SAFESTR_LEN_EXCEEDS_MAX;
	}
		
	/* hold base of pDest in case pSrc was not copied */
	orig_dmax = DestMax;
	orig_dest = pDest;
	
	if (dp < sp) {
		overlap_bumper = sp;

		/* Find the end of pDest */
		while (*dp != '\0') {

			if (dp == overlap_bumper) {
				handle_error(orig_dest, orig_dmax, "strcat_s: overlapping objects", SAFESTR_OVERLAP_UNDEF);
				return SAFESTR_OVERLAP_UNDEF;
			}

			dp++;
			DestMax--;
			if (DestMax == 0) {
				handle_error(orig_dest, orig_dmax, "strcat_s: pDest unterminated", SAFESTR_UNTERMINATED_STR);
				return SAFESTR_UNTERMINATED_STR;
			}
		}

		while (DestMax > 0) {
			if (dp == overlap_bumper) {
				handle_error(orig_dest, orig_dmax, "strcat_s: overlapping objects", SAFESTR_OVERLAP_UNDEF);
				return SAFESTR_OVERLAP_UNDEF;
			}

			*dp = *sp;
			if (*dp == '\0') {
#ifdef SAFECLIB_STR_NULL_SLACK
				/* null slack to clear any data */
				while (DestMax) { *dp = '\0'; DestMax--; dp++; }
#endif
				return SAFESTR_OK;
			}

			DestMax--;
			dp++;
			sp++;
		}

	} else {
		overlap_bumper = dp;

		/* Find the end of pDest */
		while (*dp != '\0') {

            /* NOTE: no need to check for overlap here since pSrc comes first in memory and we're not incrementing pSrc here. */
			dp++;
			DestMax--;
			if (DestMax == 0) {
				handle_error(orig_dest, orig_dmax, "strcat_s: pDest unterminated", SAFESTR_UNTERMINATED_STR);
				return SAFESTR_UNTERMINATED_STR;
			}
		}

		while (DestMax > 0) {
			if (sp == overlap_bumper) {
				handle_error(orig_dest, orig_dmax, "strcat_s: overlapping objects", SAFESTR_OVERLAP_UNDEF);
				return SAFESTR_OVERLAP_UNDEF;
			}

			*dp = *sp;
			if (*dp == '\0') {
#ifdef SAFECLIB_STR_NULL_SLACK
				/* null slack to clear any data */
				while (DestMax) { *dp = '\0'; DestMax--; dp++; }
#endif
				return SAFESTR_OK;
			}

			DestMax--;
			dp++;
			sp++;
		}
	}

    /* the entire pSrc was not copied, so null the string */
	handle_error(orig_dest, orig_dmax, "strcat_s: not enough space for pSrc", SAFESTR_NOT_ENOUGH_SPACE);

	return SAFESTR_NOT_ENOUGH_SPACE;
}
EXPORT_SYMBOL(strcat_s)
