/*------------------------------------------------------------------
 * memcpy_s
 *
 * October 2008, Bo Berry
 *
 * Copyright (c) 2008-2011 Cisco Systems
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *------------------------------------------------------------------
 */

/*******************************************************
* COPYRIGHT: B&R Industrial Automation
********************************************************
* Author:	Alex Pauls, Matt Adams
* Created:	April 8, 2020 
********************************************************
[program information]:
V1.0 - Safer replacement for memcpy that avoids 
		vulnerabilities (e.g. buffer overflows, string 
		format attacks, conversion overflows/underflows,
		etc.)
V1.2 - MJA: Included error handling with unified safe_str_constraint which causes
	   service mode in case of memory violations.
********************************************************/

#include "safeclib_private.h"
#include "mem_primitives_lib.h"
#include "SafeStr.h"
#include "safe_str_constraint.h"



/**
 * NAME
 *    memcpy_s
 *
 * SYNOPSIS
 *    #include "safe_mem_lib.h"
 *    errno_t
 *    memcpy_s(void *pDest, rsize_t DestMax, const void *pSrc, rsize_t length)
 *
 * DESCRIPTION
 *    This function copies at most length bytes from pSrc to pDest, up to
 *    DestMax. The size values are unsigned values.
 *
 *    AR: Dave - verify ISO spec requires unsigned
 *
 * SPECIFIED IN
 *    ISO/IEC JTC1 SC22 WG14 N1172, Programming languages, environments
 *    and system software interfaces, Extensions to the C Library,
 *    Part I: Bounds-checking interfaces
 *
 * INPUT PARAMETERS
 *    pDest      pointer to memory that will be replaced by pSrc.
 *
 *    DestMax      maximum length of the resulting pDest
 *
 *    pSrc       pointer to the memory that will be copied to pDest
 *
 *    length      maximum number bytes of pSrc to copy
 *
 *  OUTPUT PARAMETERS
 *    pDest      is updated
 *
 * RUNTIME CONSTRAINTS
 *    Neither pDest nor pSrc shall be a null pointer.
 *    Neither DestMax nor length shall be zero.
 *    DestMax shall not be greater than RSIZE_MAX_MEM.
 *    length shall not be greater than DestMax.
 *    Copying shall not take place between regions that overlap.
 *    If there is a runtime-constraint violation, the memcpy_s function
 *    stores zeros in the first DestMax bytes of the region pointed to
 *    by pDest if pDest is not a null pointer and length is valid.
 *
 * RETURN VALUE
 *    EOK        successful operation
 *    ESNULLP    NULL pointer
 *    ESZEROL    zero length
 *    ESLEMAX    length exceeds max limit
 *    ESOVRLP    source memory overlaps destination
 *
 * ALSO SEE
 *    memcpy16_s(), memcpy32_s(), memmove_s(), memmove16_s(),
 *     memmove32_s()
 *
 */
INT memcpy_s (UDINT pDest, UDINT DestMax, UDINT pSrc,  UDINT length)
{
    uint8_t *dp;
    const uint8_t  *sp;
	char ErrorMsg[81];
		
    dp = (uint8_t *)pDest;
    sp = (uint8_t *)pSrc;

    if (dp == NULL) {
        invoke_safe_str_constraint_handler("memcpy_s: pDest is NULL", NULL, SAFESTR_NULL_POINTER);
        return SAFESTR_NULL_POINTER;
    }

    if (DestMax == 0) {
        invoke_safe_str_constraint_handler("memcpy_s: DestMax is 0", NULL, SAFESTR_ZERO_LEN);
        return SAFESTR_ZERO_LEN;
    }

    if (DestMax > RSIZE_MAX_MEM) {    
		snprintf2(ErrorMsg, sizeof(ErrorMsg), "memcpy_s: DestMax %d > max %d", DestMax,RSIZE_MAX_MEM);
		invoke_safe_str_constraint_handler(ErrorMsg,  NULL, SAFESTR_LEN_EXCEEDS_MAX);
		return SAFESTR_LEN_EXCEEDS_MAX;
    }

    if (length == 0) {
        mem_prim_set(dp, DestMax, 0);
        invoke_safe_str_constraint_handler("memcpy_s: length is 0", NULL, SAFESTR_ZERO_LEN);
        return SAFESTR_ZERO_LEN;
    }

    if (length > DestMax) {
       	mem_prim_set(dp, DestMax, 0);
		snprintf2(ErrorMsg, sizeof(ErrorMsg), "memcpy_s: length %d > DestMax %d", length,DestMax);
		invoke_safe_str_constraint_handler(ErrorMsg,  NULL, SAFESTR_LEN_EXCEEDS_MAX);
        return SAFESTR_LEN_EXCEEDS_MAX;
    }

    if (sp == NULL) {
        mem_prim_set(dp, DestMax, 0);
        invoke_safe_str_constraint_handler("memcpy_s: pSrc is NULL", NULL, SAFESTR_NULL_POINTER);
        return SAFESTR_NULL_POINTER;
    }


    /*
     * overlap is undefined behavior, do not allow
     */
    if( ((dp > sp) && (dp < (sp+length))) ||
        ((sp > dp) && (sp < (dp+DestMax))) ) {
        mem_prim_set(dp, DestMax, 0);
		
		char *logentry[80];
		
		snprintf2((char *)logentry, sizeof(logentry)-1, "memcpy_s: overlap: dp:%i, dMax:%i, Src:%i, Len:%i",dp,DestMax,sp,length);
		
        invoke_safe_str_constraint_handler((char *)logentry, NULL, SAFESTR_OVERLAP_UNDEF);
        return SAFESTR_OVERLAP_UNDEF;
    }

    /*
     * now perform the copy
     */
    mem_prim_move(dp, sp, length);

    return SAFESTR_OK;
}
EXPORT_SYMBOL(memcpy_s)
