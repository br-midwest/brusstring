/*------------------------------------------------------------------
 * strnlen_s.c
 *
 * October 2008, Bo Berry
 *
 * Copyright (c) 2008-2011 by Cisco Systems, Inc
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *------------------------------------------------------------------
 */
/*******************************************************
* COPYRIGHT: B&R Industrial Automation
********************************************************
* Author:	Alex Pauls, Matt Adams
* Created:	April 8, 2020 
********************************************************
[program information]:
V1.0 - Safer replacement for strlen that avoids 
		vulnerabilities (e.g. buffer overflows, string 
		format attacks, conversion overflows/underflows,
		etc.).
V1.1 - MJA: Subtracts 1 from DestMax in order to allow SIZEOF() to be 
       used on strings for simple implementation
V1.2 - MJA: Included error handling with unified safe_str_constraint which causes
	   service mode in case of memory violations. Returns DestMax to be typical.
********************************************************/
#include "safeclib_private.h"
#include "SafeStr.h"
#include "safe_str_constraint.h"


UDINT strnlen_s (UDINT pDest, UDINT DestMax)
{
	uint8_t *dp;
    UDINT count;
	
	dp = (uint8_t *) pDest;

    if (dp == NULL) {
		invoke_safe_str_constraint_handler("strnlen_s: dest is 0", NULL, SAFESTR_NULL_POINTER);
        return SAFESTR_NULL_POINTER;
    }

    if (DestMax == 0) {
		invoke_safe_str_constraint_handler("strnlen_s: DestMax is 0", NULL, SAFESTR_ZERO_LEN);
        return SAFESTR_ZERO_LEN;
    }

    if (DestMax > RSIZE_MAX_STR) {
		invoke_safe_str_constraint_handler("strnlen_s: DestMax exceeds max", NULL, SAFESTR_LEN_EXCEEDS_MAX);
        return SAFESTR_LEN_EXCEEDS_MAX;
    }
	
    count = 0;
    while (*dp && DestMax) {
        count++;
        DestMax--;
        dp++;
    }

    return count;
}
EXPORT_SYMBOL(strnlen_s)
